# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'lookme_lang/version'

Gem::Specification.new do |s|
  s.name = 'lookme-lang'
  s.version = LookmeLang::VERSION
  s.authors = ['chen.zhao']
  s.summary = 'lookme lang'

  s.files = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  s.require_paths = ['lib']
end
