# lookme-lang

## 紹介

言語レベルで共通できるユティリティです。

## Preconditions

パラメータをチェック

* check_not_nil(value, message = nil)
* check_argument(condition, message = nil)

## Parameters

パラメータを解析するユティリティ

* as_int(value, default_value = nil)
* as_bool(value, default_value = false)
* as_int_array(array)
* parse_full_date(date_string)