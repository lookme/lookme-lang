module LookmeLang
  module Preconditions
    module_function

    # @param [Object] value
    # @param [String] message
    def check_not_nil(value, message = nil)
      raise InvalidArgumentError, message if value.nil?
    end

    # @param [Boolean] condition
    # @param [String] message
    def check_argument(condition, message = nil)
      raise InvalidArgumentError, message unless condition
    end
  end
end