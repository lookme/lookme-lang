module LookmeLang
  module Parameters
    module_function

    # @param [Integer,String] value
    # @param [Integer] default_value
    # @return [Integer]
    def as_int(value, default_value = nil)
      if value.nil?
        default_value
      elsif value.is_a?(Integer)
        value
      elsif value.is_a?(String)
        begin
          Integer(value)
        rescue
          default_value
        end
      else
        raise InvalidArgumentError, "unexpected value #{value.inspect}"
      end
    end

    # @param [Boolean,String] value
    # @param [Boolean] default_value
    # @return [Boolean]
    def as_bool(value, default_value = false)
      if value.nil?
        default_value
      elsif value == true || value == false
        value
      elsif value.is_a?(String)
        value.downcase == 'true'
      else
        raise InvalidArgumentError, "unexpected value #{value.inspect}"
      end
    end

    # @param [Array] array
    # @return [Array<Integer>]
    def as_int_array(array)
      array.map {|x| Integer(x)}
    end

    # @param [String] date_string
    # @return [Date]
    # @raise ArgumentError if failed to parse date
    def parse_full_date(date_string)
      return nil if date_string.nil?
      begin
        ::Date.strptime(date_string, '%Y-%m-%d')
      rescue ArgumentError => e
        raise InvalidArgumentError, e.message
      end
    end
  end
end